<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

 	public function index()
	{
		$this->load->helper('url');
		//echo base_url() ;
		$data['title'] = 'Semina';
		$this->load->view('app/header',$data);
		//$this->load->view('app/nav');
		$this->load->view('app/about');
		$this->load->view('app/footter');
	}
}
