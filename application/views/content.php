<!-- Carousel img size 800x400-->
<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
  <ol class="carousel-indicators">
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="./img/img01.png" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>First slide label</h5>
        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="./img/img02.png" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>Second slide label</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="./img/img03.png" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>Third slide label</h5>
        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </a>
</div>
<!-- End Carousel -->
<hr>
<!--Highlight --->
<div class="">
  <div class="row" style="text-align: center;">
    <h2>Highlights</h2>
    

  </div>

</div>
<!-- End Highlight -->

<hr>

<!--News -->
<div class="">
  <div class="row" style="text-align: center;">
    <h2>News</h2>
  </div>

</div>
<!--End News -->
<hr>

<!-- Contact -->

    <!--<h2>Contact</h2>-->
    <div class="row">

      <!-- Contact form -->
      <div class="col-sm-12 col-md-7">
        <div class="container">
          <h4>Contact Form</h4>
          <form>
            <div class="row">
              <div class="col-6">
                <label for="exampleInputName" class="form-label">Your name</label>
                <input type="text" class="form-control" id="exampleInputName" aria-describedby="emailHelp">
                <!--<div id="NameHelp" class="form-text">We'll never share your name with anyone else.</div>-->
              </div>
              <div class="col-6">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
              </div>
            </div>
            <div class="col-12">
              <label for="exampleInputSubject" class="form-label">Your subject</label>
              <input type="text" class="form-control" id="exampleInputSubject" aria-describedby="emailHelp">
              <!--<div id="subjectHelp" class="form-text">We'll never share your subject with anyone else.</div>-->
            </div>
            <div class="col-12">
              <label for="exampleInputMessage" class="form-label">Your messaged</label>
              <textarea type="text" class="form-control" id="exampleInputMessage"></textarea>
            </div>

            <div class="col-12" style="margin: 13px 0 0 0;">
              <button type="submit" class="col-3 btn btn-primary float-right" style="float: right;">Submit</button>
            </div>
        </form>
        </div>
      </div>
      <!-- End Contact form -->


      <!--Contact address -->
      <div class="col-sm-12 col-md-5" style="border-left: 1px solid #8080808c;">
        <div class="Container">
          <h4>address</h4>
          <div class="row">
            <div class="col-2"><i>Adress : </i> </div>
            <div class="col-10">kkldjfgkdsfjgkfdjgksdfgkfj kvdkfd</div>
          </div>

          <div class="row">
            <div class="col-2"><i>Email : </i> </div>
            <div class="col-10">testingemail@gmail.com</div>
          </div>

          <div class="row">
            <div class="col-2"><i>Tel : </i> </div>
            <div class="col-10">0987467590</div>
          </div>

          <div class="row">
            <div class="col-2"><i>Fax : </i> </div>
            <div class="col-10">0987467590</div>
          </div>
        </div>
      </div>
      <!--End Contact address -->
  </div>
<!--End Cntact -->
