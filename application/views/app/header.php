<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html5>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title;?></title>
    <link href="./assete/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="./assete/css/header-footer.css" rel="stylesheet">
    <!-- image slide -->
    <link href="./assete/css/slick.css" rel="stylesheet" type="text/css" >
  	<!--<link href="./assete/css/slick-theme.css" rel="stylesheet" type="text/css" >-->
	

  </head>
  <body>

    <div class="container-fluid"><!-- Container -->
    	<!--nav-->
    	<div style="background-image: url('<?php echo site_url("/img/title_h100px.jpg");?>'); padding:.5rem 0;background-size:cover;">
	    	<div class="container"  style=" max-width: 94%;">
    			<div class="row">
				    <div class="col-sm-2 col-md-2 col-lg-2 "  style="padding: .1rem 0;">
				      	<a class="nav-link " href="<?php echo site_url();?>" style="padding: .5rem 0rem;">
				          	<img src="<?php echo site_url("/img/saradee_logo.png");?>" style=" width: 100%;">
				      	</a>
				    </div>
				    <div class="col-sm col-md col-lg">
				      <div class="row" style="padding: 1rem 0 0 0;">
					    <div class="col-sm-11 col-md-10 col-lg-10">
						    <ul style=" list-style-type: none;  margin: 0;padding: 0;overflow: hidden;float: right;">
							  <li style="float: left;">
							  	<a style="display: inline-block;color: white;text-align: center;padding: 0 25px;text-decoration: none;" href="<?php echo site_url();?>" class="active">HOME
							  	</a>
							  </li>
							  <li style="float: left;">
							  	<div style="padding: .4rem 0 0 0;">
						          	<img src="<?php echo base_url("/img/I.png");?>" style=" width: 2px;height: .7rem;">
						      	</div>
							  </li>
							  <li style="float: left;">
							  	<a style="display: inline-block;color: white;text-align: center;padding: 0 20px;text-decoration: none;" href="<?php echo site_url('shedule');?>" class="active">SCHEDULE
							  	</a>
							  </li>
							  <li style="float: left;">
							  	<div style="padding: .4rem 0 0 0;">
						          	<img src="<?php echo base_url("/img/I.png");?>" style=" width: 2px;height: .7rem;">
						      	</div>
							  </li>
							  <?php
							  //echo '.... ';
							  //echo base_url('/');exit();
							  ?>
							  <li style="float: left;">
							  	<a style="display: inline-block;color: white;text-align: center;padding: 0 20px;text-decoration: none;" href="<?php echo base_url('live');?>" class="active">LIVE TV
							  	</a>
							  </li>
							  <li style="float: left;">
							  	<div style="padding: .4rem 0 0 0;">
						          	<img src="<?php echo base_url("/img/I.png");?>" style=" width: 2px;height: .7rem;">
						      	</div>
							  </li>
							  <li style="float: left;">
							  	<a style="display: inline-block;color: white;text-align: center;padding: 0 20px;text-decoration: none;" href="<?php echo site_url('vod');?>" class="active">VOD
							  	</a>
							  </li>
							  <li style="float: left;">
							  	<div style="padding: .4rem 0 0 0;">
						          	<img src="<?php echo base_url("/img/I.png");?>" style=" width: 2px;height: .7rem;">
						      	</div>
							  </li>
							  <li style="float: left;">
							  	<a style="display: inline-block;color: white;text-align: center;padding: 0 20px;text-decoration: none;" href="<?php echo site_url('news');?>" class="active">NEWS
							  	</a>
							  </li>
							  <li style="float: left;">
							  	<div style="padding: .4rem 0 0 0;">
						          	<img src="<?php echo base_url("/img/I.png");?>" style=" width: 2px;height: .7rem;">
						      	</div>
							  </li>
							  <li style="float: left;">
							  	<a style="display: inline-block;color: white;text-align: center;padding: 0 20px;text-decoration: none;" href="<?php echo site_url('contact');?>" class="active">CONTACT US
							  	</a>
							  </li>
							</ul>
					    </div>
					    <div class="col-sm-1 col-md-2 col-lg-2">
					       <a class="nav-link border-menu-language" href="#" style="float: right; padding: inherit;">
				          	<img src="<?php echo base_url("/img/usa.png");?>" style=" height: 1.5rem; margin-right:3px; ">EN
				          </a>
					    </div>
					  </div>
				    </div>
			    </div>
			   
			</div>
		</div>
		<div  style=" border-bottom: 1px solid #dedede; background-color: #a50a09; padding: .5rem 0 .5rem 0;border-right: 1px solid white; border-left: 1px solid white;">
			<div class="container"  style=" max-width: 94%;">
				<div class="row  justify-content-between">
					<div class="col-sm-8 col-md-9 col-lg-9 "></div>
					<div class="col-sm-4 col-md-3 col-lg-3">
						<div class="row  align-self-end">
				            <div class="col"></div>
				              <div class="col-sm-3 col-md-2" style="padding-right: inherit;">
				                <a href="" >
				                  <img src="<?php echo site_url("/img/fb_icon.png");?>" style=" float: right;height: 1.5rem;">
				                </a>
				              </div>
				              <div class="col-sm-3 col-md-2" style="padding-right: inherit;">
				                <a href="" >
				                  <img src="<?php echo site_url("/img/ig_icon.png");?>" style=" float: right;height: 1.5rem;">
				                </a>
				              </div>
				              <div class="col-sm-3 col-md-2" style="padding-right: inherit;">
				                <a href="" >
				                  <img src="<?php echo site_url("/img/yt_icon.png");?>" style=" float: right;height: 1.5rem;">
				                </a>
				              </div>
				          </div>
					</div>
				</div>
			</div>
		</div>
		<!-- End nav -->
	</div>
