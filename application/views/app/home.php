
    <div class="container-fluid contents">
      
      <!-- Carousel img size 800x400-->
      
      <!--<div class="row">
        <section class="lazy slider" data-sizes="50vw">
        <div><img src="<?php echo site_url("/img/poster1-1020x500.jpg");?>" class="d-block w-100" alt="..."></div>
        <div><img src="<?php echo site_url("/img/poster2-1020x500.png");?>" class="d-block w-100" alt="..."></div>
      </section>
      </div>-->

      <!--<div id="poster-carousel" class="carousel slide" data-bs-ride="carousel"  style=" border-bottom: 1px solid #7e807e;">
        <ol class="carousel-indicators">
          <li data-bs-target="#poster-carousel" data-bs-slide-to="0" class="active"></li>
          <li data-bs-target="#poster-carousel" data-bs-slide-to="1"></li>
          <li data-bs-target="#poster-carousel" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="<?php echo site_url("/img/poster1-1020x500.jpg");?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>First slide label</h5>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?php echo site_url("/img/poster2-1020x500.png");?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Second slide label</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#poster-carousel" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </a>
        <a class="carousel-control-next" href="#poster-carousel" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </a>
      </div>-->
      <!-- End Carousel -->


      <div class="container" style="max-width: 94%;">
        <div class="row">
          <hr class="nhr">
        </div>
        <div class="row" style="text-align: left; color: white; ">
          <h4 style=" padding-left: 0rem !important;">NEWS RELEASE</h4>
        </div>
        <div class="row">
          <div class="container overflow-hidden" style="padding:0;">
            <div class="regular slider">
              <div style="padding: 0 1rem 0 0 !important">
                <!-- Collaps <div type="button" onclick="getvalue(1);">-->
                <div type="button" data-bs-toggle="modal" data-bs-target="#new-release-collaps-1" onclick="getvalue(1);">
                  <img src="<?php echo site_url("/img/movie1.jpeg");?>" style="position:relative;z-index:1;width:100%!important;"/>
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-1" value="1">
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <div type="button" data-bs-toggle="modal" data-bs-target="#new-release-collaps-2" onclick="getvalue(2);">
                  <img src="<?php echo site_url("/img/mov2.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-2" value="2">
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <div type="button" data-bs-toggle="collapse" href="#new-release-collaps" role="button" aria-expanded="false" aria-controls="new-release-collaps" onclick="getvalue(3);">
                  <img src="<?php echo site_url("/img/mov3.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-3" value="3">
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <div type="button" data-bs-toggle="collapse" href="#new-release-collaps" role="button" aria-expanded="false" aria-controls="new-release-collaps" onclick="getvalue(4);">
                  <img src="<?php echo site_url("/img/mov4.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-4" value="4">
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <div type="button" data-bs-toggle="collapse" href="#new-release-collaps" role="button" aria-expanded="false" aria-controls="new-release-collaps" onclick="getvalue(5);">
                  <img src="<?php echo site_url("/img/mov5.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: 0.2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-5" value="5">
              </div >
              <div style="padding: 0 1rem 0 0 !important">
                <div type="button" data-bs-toggle="collapse" href="#new-release-collaps" role="button" aria-expanded="false" aria-controls="new-release-collaps" onclick="getvalue(6);">
                  <img src="<?php echo site_url("/img/mov6.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-6" value="6">
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <div type="button" data-bs-toggle="collapse" href="#new-release-collaps" role="button" aria-expanded="false" aria-controls="new-release-collaps" onclick="getvalue(7);">
                  <img src="<?php echo site_url("/img/mov7.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-7" value="7">
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <div type="button" data-bs-toggle="collapse" href="#new-release-collaps" role="button" aria-expanded="false" aria-controls="new-release-collaps" onclick="getvalue(8);">
                  <img src="<?php echo site_url("/img/mov8.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </div>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
                <input type="hidden" id="detail-8" value="8">
              </div>
            </div>
          </div>
        </div>
      </div>

      
      <!-- Detail collaps -->

      <div class="container-fluid">
        <div class="modal fade" id="new-release-collaps-1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >
          <div class="modal-dialog" style="margin: 7% auto; max-width: 75%;">
            <div class="modal-content" style="background-color: #af0404; color: white; border-radius:inherit;">
              
              <div class="container" style=" max-width: 94%;">
                <div class="row" style="padding: .5rem 0;">
                  <div class="col-sm-11 col-md-11 col-lg-11" style="">DETAIL</div>
                  <div class="col-sm-1 col-md-1 col-lg-1" style="">
                    <div type="button" id="new-release-close-collaps" data-bs-dismiss="modal" aria-label="Close">
                      <img src="<?php echo site_url("/img/close.png");?>" style=" height: 1.5rem;float: right;">
                      <!--<label style="float: right;padding: 0 5px 0 0;cursor: pointer;">CLOSE</label>-->
                    </div>
                  </div>
                </div>
              </div>
              <div class="container">
                <div style="background-color: #191919;background-size:cover;">
                <!--<div style="background-image: url(<?php echo site_url("/img/movie1.jpeg");?>);background-size:cover;">-->
                  <div class="container" style=" max-width: 94%; padding: 1rem 0rem;">
                    <div class="row">
                      <iframe width="560" height="315" src="https://www.youtube.com/embed/nPmIhH775L4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                  </div>
                </div>
              </div>
              <div class="container" style=" padding: 1rem 0rem;">
                <div class="container" style=" max-width: 94%;"><div class="row"></div></div>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="collapse" id="new-release-collaps"  style="background-color: #af0404; color: white;">
          <div class="row">
            <div class="container" style=" max-width: 94%; padding: 1rem 0rem;">
              <div class="row">
                <div class="col-sm col-md col-lg" style="">DETAIL</div>
                 <div class="col-sm col-md col-lg" style="">
                  <div class="row">
                    <div class="col-sm-11 col-md-11 col-lg-11"></div>
                    <div class="col-sm-1 col-md-1 col-lg-1" >
                      <div class="row">
                        <div type="button" id="new-release-close-collaps">
                          <img src="<?php echo site_url("/img/close.png");?>" style=" height: 1.5rem;float: right;}">-->
                          <!--<label style="float: right;padding: 0 5px 0 0;cursor: pointer;">CLOSE</label>-->
                        <!--</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
          <div class="row">
              <div class="container" style=" max-width: 94%; padding: 1rem 0rem;">
                <div id="new-release-detail-collapse" class="" style=" border: indianred; border-radius: inherit;">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                  </div>
              </div>
          </div>
        </div>-->
      </div>
            <div class="container-fluid">
        <div class="modal fade" id="new-release-collaps-2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >
          <div class="modal-dialog" style="margin: 7% auto; max-width: 75%;">
            <div class="modal-content" style="background-color: #af0404; color: white; border-radius:inherit;">
              
              <div class="container" style=" max-width: 94%;">
                <div class="row" style="padding: .5rem 0;">
                  <div class="col-sm-11 col-md-11 col-lg-11" style="">DETAIL</div>
                  <div class="col-sm-1 col-md-1 col-lg-1" style="">
                    <div type="button" id="new-release-close-collaps" data-bs-dismiss="modal" aria-label="Close">
                      <img src="<?php echo site_url("/img/close.png");?>" style=" height: 1.5rem;float: right;">
                    </div>
                  </div>
                </div>
              </div>
              <div class="container">
                <div style="background-color: #191919;background-size:cover;">
                  <div class="container" style=" max-width: 94%; padding: 1rem 0rem;">
                    <div class="row">
                      <iframe width="560" height="315" src="https://www.youtube.com/embed/S8twVZf2XYs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                  </div>
                </div>
              </div>
              <div class="container" style=" padding: 1rem 0rem;">
                <div class="container" style=" max-width: 94%;"><div class="row"></div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
      <!-- End detail collaps -->


      <!-- NEWS RELEASE -->
      <!--<div class="container" style=" max-width: 94%;">
        <div class="row">
          <hr class="nhr">
        </div>
        <div class="row" style="text-align: left; color: white; ">
          <h4 style=" padding-left: 0rem !important;">NEWS RELEASE</h4>
        </div>
        <div class="row" style="">
          <div class="container overflow-hidden" style="padding:0;">
            <div class="regular slider">
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/movie1.jpeg");?>" style="position: relative; z-index: 1;width: 100%!important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov2.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov3.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov4.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov5.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: 0.2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div >
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov6.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov7.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov8.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>-->
      <!-- ENd NEWS RELEASE -->



      <!-- POPUPLATION -->
      <div class="container"  style=" max-width: 94%;">
        <div class="row">
          <hr class="nhr">
        </div>
        <div class="row" style="text-align: left; color: white; ">
          <h4 style=" padding-left: 0rem !important;">POPUPLATION</h4>
        </div>
        <div class="row" style="">
          <div class="container overflow-hidden" style="padding:0;">
            <div class="regular slider">
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/movie1.jpeg");?>" style="position: relative; z-index: 1;width: 100%!important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov2.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov3.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div  style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov4.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                  <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov5.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: 0.2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div >
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov6.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov7.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
              <div style="padding: 0 1rem 0 0 !important">
                <a href="#!">
                  <img src="<?php echo site_url("/img/mov8.jpg");?>" style="position: relative; z-index: 1;width: 100% !important;" />
                </a>
                <div style="background-color:#af0404; color: white;font-size: 15px;padding: .2rem;height: 4rem;">
                   <div class="row">
                    <div class="col">MOVIE NAME</div>
                    <div class="col-2">
                      <img src="<?php echo site_url("/img/!.png");?>" style=" height: 1.5rem;float: right;}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--END POPUPLATION-->

      <!-- POPUPLATION -->
      <div class="container"  style=" max-width: 94%;">
        <div class="row">
          <hr class="nhr">
        </div>
        <!-- topic-->
        <div class="row" style="text-align: left; color: white; ">
          <h4 style=" padding-left: 0rem !important;">POPUPLATION</h4>
        </div>
        <!-- end topic -->
        <!-- detail-->
        <div class="row" style="">
          <div class="container overflow-hidden" style="padding: 1rem 0;">
            <div class="row">
              <div class="col-sm-3 col-md-3 col-lg-3">
                <div class="bg-image hover-overlay ripple">
                  <a href="#!">
                    <img src="<?php echo site_url("/img/promote-400x450.jpg");?>" class="img-fluid" style=""/>
                  </a>
                  <div style="background-color:#af0404; color: white;font-size: 15px;padding: 5px;height: 3.5rem;">
                    <div class="row">
                      <div class="col-12">MEERKAT MANOR THE NEXT GENERATION</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-9 col-md-9 col-lg-9">
                <div class="row" style="color: white;">
                  <div class="col-3" style="padding: 0 0 0 0.3rem !important;">
                    <div class="card" style="border-radius: inherit;">
                      <img src="<?php echo site_url("/img/family-250x200.jpg");?>" class="img-fluid" style="">
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>Attending 'Star Wars' Screening in 'Star Trek' Costume</h6>
                    <p style="font-size: smaller;">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <p  style="font-size: small; color: gray;margin-bottom: 0rem !important;">With supporting text below as a natural lead-in to additional content</p>
                  </div>
                  <div class="col-1" style=" background-color: #af0404;padding-top: 8%; padding-left: 2%;">
                    <img src="<?php echo site_url("/img/next_icon.png");?>" style="height: 2rem;">
                  </div>
                </div>

                <div class="row" style="color: white; margin-top: 1rem;">
                  <div class="col-3"  style="padding: 0 0 0 .3rem !important;">
                    <div class="card" style="border-radius: inherit;">
                      <img src="<?php echo site_url("/img/family-250x200.jpg");?>" class="img-fluid" style="">
                    </div>
                  </div>
                  <div class="col-8">
                    <h6>Attending 'Star Wars' Screening in 'Star Trek' Costume</h6>
                    <p style="font-size: smaller;">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <p  style="font-size: small; color: gray;margin-bottom: 0rem !important;">With supporting text below as a natural lead-in to additional content</p>
                  </div>
                  <div class="col-1" style=" background-color: #af0404;padding-top: 8%;padding-left: 2%;">
                    <img src="<?php echo site_url("/img/next_icon.png");?>" style="height: 2rem;">
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!--end detail-->
      </div>
      <!--END POPUPLATION-->

    </div>
    
