<!-- footer -->
<div class="container-fluid" style="background-image: url('<?php echo site_url("/img/footer_h300px.jpg");?>'); background-size: cover;">

  <div class="container"  style=" max-width: 94%;">
    <div class="row" style="color: white; padding-top: 1.5rem; font-size: 13px;">
      <p style="margin-bottom: .3rem;padding-left: 0rem;">พีเอสไอสาระดี "เปิดโลกกว้างอย่างไร้ขีดจำกัด"</p>
      <p style="margin-bottom: .3rem;padding-left: 0rem;">ที่อยู่ : 588 ซอยศรีนครินทร์ 16 ถนนศรีนครินทร์</p>
      <p style="margin-bottom: .3rem;padding-left: 0rem;">แขวงพัฒนาการ เขตสวนหลวง กรุงเทพ 10250</p>
      <p style="padding-left: 0rem;">ติดต่อโฆษณา 092-914-614</p>
    </div>

    <div class="row">
      <img src="./img/saradee_logo.png" style="width: 13rem;padding-left: 0rem;">
    </div>

    <div class="row">
      <div class="col-sm-8 col-md-9 col-lg-9"  style="color: white; font-size: 11px;padding-left: 0rem;">
        <p>Copyright 2019 PSI SARADEE co., Itd. All right reserved</p>
      </div>
      <div class="col-sm-4 col-md-3 col-lg-3">
          <div class="row  align-self-end">
            <div class="col"></div>
              <div class="col-sm-3 col-md-2" style="padding-right: inherit;">
                <a href="" >
                  <img src="<?php echo site_url("/img/fb_icon.png");?>" style=" float: right;height: 1.5rem;">
                </a>
              </div>
              <div class="col-sm-3 col-md-2" style="padding-right: inherit;">
                <a href="" >
                  <img src="<?php echo site_url("/img/ig_icon.png");?>" style=" float: right;height: 1.5rem;">
                </a>
              </div>
              <div class="col-sm-3 col-md-2" style="padding-right: inherit;">
                <a href="" >
                  <img src="<?php echo site_url("/img/yt_icon.png");?>" style=" float: right;height: 1.5rem;">
                </a>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
        <!--nav-->
<div class="container-fluid" style="text-align: center;">
  <ul style=" list-style-type: none; margin: 0;padding: 1rem 0;overflow: hidden;text-align: center;display: inline-flex;">
    <li>
      <a style="display: inline-block;color: #333333;text-align: center;padding: 0 35px;text-decoration: none;" href="#home" class="active">HOME
      </a>
    </li>
    <li>
      <div style="margin-top: 0.4rem;height: .8rem;background-color: #333333;">
         <div style="width: 0.1rem;"></div>
      </div>
    </li>
    <li>
      <a style="display: inline-block;color: #333333;text-align: center;padding: 0 35px;text-decoration: none;" href="#home" class="active">SCHEDULE
      </a>
    </li>
    <li>
      <div style="margin-top: 0.4rem;height: .8rem;background-color: #333333;">
         <div style="width: 0.1rem;"></div>
      </div>
    </li>
    <li>
      <a style="display: inline-block;color: #333333;text-align: center;padding: 0 35px;text-decoration: none;" href="#home" class="active">LIVE TV
      </a>
    </li>
    <li>
      <div style="margin-top: 0.4rem;height: .8rem;background-color: #333333;">
         <div style="width: 0.1rem;"></div>
      </div>
    </li>
    <li>
      <a style="display: inline-block;color: #333333;text-align: center;padding: 0 35px;text-decoration: none;" href="#home" class="active">VOD
      </a>
    </li>
    <li>
      <div style="margin-top: 0.4rem;height: .8rem;background-color: #333333;">
         <div style="width: 0.1rem;"></div>
      </div>
    </li>
    <li>
      <a style="display: inline-block;color: #333333;text-align: center;padding: 0 35px;text-decoration: none;" href="#home" class="active">NEWS
      </a>
    </li>
    <li>
      <div style="margin-top: 0.4rem;height: .8rem;background-color: #333333;">
         <div style="width: 0.1rem;"></div>
      </div>
    </li>
    <li>
      <a style="display: inline-block;color: #333333;text-align: center;padding: 0 35px;text-decoration: none;" href="#home" class="active">CONTACT US
      </a>
    </li>
  </ul>
</div>

<!-- End footer -->




<script src="./assete/js/jquery.js" ></script>
<script src="./assete/js/bootstrap.bundle.min.js" ></script>

<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="./assete/js/slick.js" type="text/javascript" charset="utf-8"></script>


<script type="text/javascript">
  /*const carouselEl = document.querySelector('#poster-carousel')
  const carousel = new bootstrap.Carousel(carouselEl, {
    interval: 7000,
    wrap: true,
    touch:true
  });*/

  /*const new_release_carouselEL = document.querySelector('#new-release-carousel')
  const new_release_carousel = new bootstrap.Carousel(new_release_carouselEL, {
    interval: 2000,
    wrap: true,
    touch:true
  },3);*/

  $(".lazy").slick({
    lazyLoad: 'ondemand', // ondemand progressive anticipated
    infinite: true,
    prevArrow :'',
    nextArrow :'',
  });

  $(".regular").slick({
    focusOnSelect:false,
    dots: false,
    infinite: true,
    prevArrow :'',
    nextArrow :'',
    touchMove:true,
    slidesToShow: 4,
    slidesToScroll: 4
  });



function getvalue(val){
  //$("#new-release-collaps").slideToggle(500, function () {});
}

// button close  collapse
/*$("#new-release-close-collaps").click(function(){
  $("#new-release-collaps").collapse('hide');
});*/
    
</script>
</body>
</html>
